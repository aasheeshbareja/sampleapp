
**Project Structure used for Node js App**

controllers/ – defines your app routes and their logic

helpers/ – code and functionality to be shared by different parts of the project

middlewares/ – Express middlewares which process the incoming requests before handling them down to the routes

node_modules/ - For managing npm dependencies

models/ – represents data, implements business logic

public/ – contains all static files like images, styles and javascript

views/ – provides templates which are rendered and served by your routes

tests/ – tests everything which is in the other folders

app.js – initializes the app.

package.json – manage package versions
